package es.inetum.practica0.model.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.inetum.practica0.model.Papel;
import es.inetum.practica0.model.Piedra;
import es.inetum.practica0.model.PiedraPapelTijeraFactory;
import es.inetum.practica0.model.Tijera;

class PiedraPapelTijeraTest2 {

	Piedra piedra;
	Papel papel;
	Tijera tijera;
	
	
	@BeforeEach
	void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}
	
	@BeforeEach
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
	}

	@Test
	void testGetInstance() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA).getNombre().toLowerCase());
	}
	
	@Test
	void testCompararTijeraConPapel() {
		assertEquals(1, tijera.comparar(papel));
	}
	
	@Test
	void testCompararTijeraConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
	}

}
