package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;
import com.domain.util.ConnectionManager;

public class AlumnoDao implements DAO {

	public AlumnoDao() {
		
	}

	public void agregar(Model pModel) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		String sql = new String("insert into alumnos(alu_nombre, alu_apellido, alu_conocimientos,alu_git) values(?,?,?,? )");
		
		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkArepositorio());		
		stm.execute();
		
	}

	public void modificar(Model pModel) throws ClassNotFoundException, SQLException {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		String sql = new String("update alumnos set alu_nombre=?, alu_apellido=?, alu_conocimientos=?, alu_git=? where alu_id=?");
		
		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkArepositorio());
		stm.setInt(5, alu.getCodigo());
		stm.execute();

	}

	public void eliminar(Model pModel) throws ClassNotFoundException, SQLException {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		String sql = new String("DELETE FROM alumnos where ALU_ID=?");
		
		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setInt(1, alu.getCodigo());
		stm.execute();

	}

	public List<Model> leer(Model pModel) throws ClassNotFoundException, SQLException {
		
		List<Model> listado = new ArrayList<Model>();
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		Alumno alu =(Alumno) pModel;
		//vamos a montar un filtro con todos los valores que nos vengan informados
		StringBuilder sb = new StringBuilder("Select * from alumnos where 1=1");
		if (alu.getCodigo() > 0) {
			sb.append(" AND ALU_ID = " + alu.getCodigo());
		}
		
		if (StringUtils.isNotEmpty(alu.getNombre())) {
			sb.append(" AND ALU_NOMBRE = '" + alu.getNombre() + "'");
		}

		if (StringUtils.isNotEmpty(alu.getApellido())) {
			sb.append(" AND ALU_APELLIDO = '" + alu.getApellido() + "'");
		}
		
		if (StringUtils.isNotEmpty(alu.getEstudios())) {
			sb.append(" AND ALU_CONOCIMIENTOS = '" + alu.getEstudios() + "'");
		}
		
		if (StringUtils.isNotEmpty(alu.getLinkArepositorio())) {
			sb.append(" AND ALU_GIT = '" + alu.getLinkArepositorio() + "'");
		}
		
		PreparedStatement stmt = con.prepareStatement(sb.toString());
		ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				Alumno aluLeido = new Alumno(rs.getInt("ALU_ID"), rs.getString("ALU_NOMBRE"), rs.getString("ALU_APELLIDO"),
						rs.getString("ALU_CONOCIMIENTOS"), rs.getString("ALU_GIT"));
				listado.add(aluLeido);
			}
			
		
		return listado;
	}

}
