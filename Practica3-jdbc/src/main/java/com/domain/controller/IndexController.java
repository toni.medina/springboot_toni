package com.domain.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.domain.modelo.Model;
import com.domain.modelo.dao.AlumnoDao;
import com.domain.modelo.dao.DAO;

@Controller
public class IndexController {
	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}

	@RequestMapping("/")
	public String getPresentacion() {
		return "Prersentacion";
	}

	@RequestMapping("/listado")
	public String goListado(Model model) {
		List<com.domain.modelo.Model> alumnos = null;
		DAO aluDao = new AlumnoDao();
		try {
			alumnos = aluDao.leer(null);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);

		return "listado";
	}
}
