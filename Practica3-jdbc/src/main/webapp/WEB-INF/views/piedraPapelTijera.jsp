<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Juego de Piedra, Papel, Tijera, Lagarto y Spock</title>
</head>
<body>
	<form action="resolverJuego" method="GET">
		<select name="selJugador">
			<c:forEach items="${opciones}" var="opcion">
				<option value="${opcion.numero}">${opcion.nombre}</option>
			</c:forEach>
		</select>
		<input type="submit" value="env�a">
	</form>
</body>
</html>