package es.inetum.domain.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.html.HTML;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.HtmlUtils;

import es.inetum.practica0.model.PiedraPapelTijeraFactory;

@Controller
public class IndexController {

	@RequestMapping("/home")
	public String goIndex() {
		return "index";
	}

	@RequestMapping("/juego")
	public String goListado(Model model) {
		// crear un arrayList con las opciones piedra, papel y tijera
		List<PiedraPapelTijeraFactory> opciones = obtenerListadoOpciones();

		model.addAttribute("opciones", opciones);
		return "piedraPapelTijera";
	}

	@RequestMapping("/resolverJuego")
	public String goResultado(@RequestParam(required = false) Integer selJugador, Model model) {

		// seleccion de la m�quina
		PiedraPapelTijeraFactory maquina = PiedraPapelTijeraFactory.getInstance(escogerValorAleatorio());
		// materializamos la selecci�n que hizo el usuario
		PiedraPapelTijeraFactory opcionUsuario = PiedraPapelTijeraFactory.getInstance(selJugador.intValue());

		// comparamos
		opcionUsuario.comparar(maquina);

		// montamos mensaje de respuesta
		String mensaje = String.format("El usuario escogi� '%s' y la m�quina '%s'. El resultado es: %s",
				opcionUsuario.getNombre(), maquina.getNombre(), opcionUsuario.getDescripcionREsultado());

		model.addAttribute("mensaje", HtmlUtils.htmlEscape(mensaje));
		return "resultadoJuego";
	}

	private int escogerValorAleatorio() {
		// Vamos a devolver un �ndice entre 1 y 5 que son los valores que identifican a:
		// Piedra, Papel, Tijera, Lagarto y Spock
		return (int) ((Math.random() * 100) % 5 + 1);
	}

	private List<PiedraPapelTijeraFactory> obtenerListadoOpciones() {
		List<PiedraPapelTijeraFactory> opciones = new ArrayList<PiedraPapelTijeraFactory>();

		/*
		 * OPCI�N A: a saco sin usar patr�n (ojo!!! faltar�n los imports si se habilita
		 * esta opci�n) opciones.add(new Piedra()); opciones.add(new Papel());
		 * opciones.add(new Tijera()); opciones.add(new Lagarto()); opciones.add(new
		 * Spock());
		 * 
		 */

		/*
		 * OPCI�N B: usando el patr�n de dise�o
		 */
		opciones.add(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA));
		opciones.add(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL));
		opciones.add(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA));
		opciones.add(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO));
		opciones.add(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK));

		return opciones;
	}

}
