package com.inetum.tonicode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inetum.tonicode.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {
	Usuario findByNombre(String nombre);
}
