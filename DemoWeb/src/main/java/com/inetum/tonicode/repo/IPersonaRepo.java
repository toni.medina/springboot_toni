package com.inetum.tonicode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inetum.tonicode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
