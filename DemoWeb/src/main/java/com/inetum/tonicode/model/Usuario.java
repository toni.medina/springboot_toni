package com.inetum.tonicode.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	private int codigo;
	private String nombre;
	private String password;
	
	public Usuario() {
		
	}
	
	public Usuario(int codigo, String nombre, String password) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.password = password;
	}



	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
