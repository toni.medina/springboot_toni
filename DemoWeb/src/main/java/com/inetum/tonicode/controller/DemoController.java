package com.inetum.tonicode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.inetum.tonicode.model.Persona;
import com.inetum.tonicode.repo.IPersonaRepo;

@Controller
public class DemoController {
	@Autowired
	private IPersonaRepo repo;
	
	@GetMapping("/save")
	public String save(@RequestParam(name="name", required=true, defaultValue="<NO_NAME>") String name, Model model) {
		
		Persona persona = new Persona(name);
		repo.save(persona);
		
		model.addAttribute("name", name);
		return "greeting";
	}
	
	@GetMapping("/all")
	public @ResponseBody Iterable<Persona> findAllPersons(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		
		return repo.findAll();
		
	}
}
