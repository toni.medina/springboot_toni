package com.inetum.tonicode.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.inetum.tonicode.model.Usuario;
import com.inetum.tonicode.repo.IUsuarioRepo;

@Service
public class UserService {
	@Autowired
	private IUsuarioRepo repo;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = repo.findByNombre(username);
		
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("admin"));
		
		UserDetails userDetail = new User(usuario.getNombre(), usuario.getPassword(), roles);
		return userDetail;
	}
}
