package com.inetum.tonicode.repo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.inetum.tonicode.model.Usuario;

@SpringBootTest
class IUsuarioRepoTest {

	@Autowired
	IUsuarioRepo repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@BeforeEach
	void setUp() throws Exception {
	}
	
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * 1ª versión:
	 * SIN password ENCODER 
	 */
//	@Test
//	void testAgregar() {
//		Usuario usu = new Usuario(0, "Toni", "medina");
//		Usuario user = repo.save(usu);
//		assertEquals("medina", user.getPassword());
//	}
	
	@Test
	void testAgregar() {
		Usuario usu = new Usuario(0, "Toni", encoder.encode("medina"));
		Usuario user = repo.save(usu);
		assertTrue(encoder.matches("medina", user.getPassword()));
	}

}
